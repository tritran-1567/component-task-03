<?php $pageid="form6";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-form">
	<p class="pc-only" style="text-align:center; margin: 100px; font-size:3vw">Please change mobile, Thank you !!</p>
	<section class="p-form__6 sp-only">
		<form action method="post">
			<div class="l-container">
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-text1">
					<p>Lorem ipsum : <span>211-0014</span></p>
                </div>
                <div class="c-select1">
					<label>
						<select>
                            <option value selected disabled>Lorem, ipsum dolor.</option>
							<option value="value1">Value 1</option>
							<option value="value2">Value 2</option>
							<option value="value3">Value 3</option>
							<option value="value4">Value 4</option>
							<option value="value5">Value 5</option>
						</select>
					</label>
                </div>
                <div class="p-form__group">
                    <div class="c-btn2">
                        <a href=""><span>Cancel</span></a>
                    </div>
                    <div class="c-btn1">
                        <input type="submit" value="OK">
                    </div>
                </div>
			</div>
		</form>
	</section>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
