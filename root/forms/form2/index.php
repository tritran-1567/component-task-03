<?php $pageid="form2";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-form">
	<p class="pc-only" style="text-align:center; margin: 100px; font-size:3vw">Please change mobile, Thank you !!</p>
	<section class="p-form__2 sp-only">
		<form action method="post">
			<div class="l-container">
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-note1">
					<p>Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.</p>
				</div>
				<div class="c-ttl1 c-ttl1--red">
					<h3>Lorem, ipsum. <img src="/assets/img/common/icon1.png" alt=""></h3>
				</div>
				<div class="c-input3">
					<input type="number" placeholder="1234567">
					<div class="c-btn2">
						<a href=""><span>Lorem ipsum</span></a>
					</div>
				</div>
				<div class="c-note1">
					<p>Lorem ipsum</p>
				</div>
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-select1">
					<label>
						<select>
							<option value="value1">Value 1</option>
							<option value="value2">Value 2</option>
							<option value="value3">Value 3</option>
							<option value="value4">Value 4</option>
							<option value="value5">Value 5</option>
						</select>
					</label>
				</div>
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-input1">
					<input type="text" placeholder="Input">
				</div>
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-input1">
					<input type="text" placeholder="Input">
				</div>
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-input1">
					<input type="text" placeholder="Input">
				</div>
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-select1">
					<label>
						<select>
							<option value="value1">Value 1</option>
							<option value="value2">Value 2</option>
							<option value="value3">Value 3</option>
							<option value="value4">Value 4</option>
							<option value="value5">Value 5</option>
						</select>
					</label>
				</div>
				<div class="c-input2">
					<input type="tel" placeholder="012">
					<input type="tel" placeholder="345">
					<input type="tel" placeholder="6789">
				</div>
				<div class="c-note1">
					<p>Lorem ipsum</p>
				</div>
				<div class="c-box1">
					<div class="c-note1">
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem unde consequuntur nisi.</p>
					</div>
					<div class="c-ttl1">
						<h3>Lorem, ipsum.</h3>
					</div>
					<div class="c-input6">
						<input type="number" placeholder="12">
						<input type="number" placeholder="123">
						<input type="number" placeholder="12">
						<input type="number" placeholder="12">
						<input type="number" placeholder="12">
						<input type="number" placeholder="123456">
					</div>
					<div class="c-note1">
						<p>Lorem ipsum dolor</p>
					</div>
					<div class="c-ttl1">
						<h3>Lorem, ipsum.</h3>
					</div>
					<div class="c-input5">
						<input type="number" placeholder="12">
						<input type="number" placeholder="123">
						<input type="number" placeholder="1234">
						<input type="number" placeholder="1234">
						<input type="number" placeholder="1234">
						<input type="number" placeholder="1234">
					</div>
					<div class="c-note1">
						<p>Lorem ipsum dolor</p>
					</div>
				</div>
				<div class="c-btn1">
					<input type="submit" value="OK">
				</div>
			</div>
		</form>
	</section>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
