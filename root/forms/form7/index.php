<?php $pageid="form7";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-form">
	<p class="pc-only" style="text-align:center; margin: 100px; font-size:3vw">Please change mobile, Thank you !!</p>
	<section class="p-form__7 sp-only">
		<form action method="post">
			<div class="l-container">
				<div class="p-form__box1">
					<div class="c-ttl1 is-required">
						<h3>Lorem, ipsum.</h3>
						<a href=""><span>修正</span></a>
					</div>
					<p>Lorem ipsum dolor sit.</p>
					<div class="c-ttl1">
						<h3>Lorem, ipsum.</h3>
					</div>
					<p>Lorem ipsum dolor sit.</p>
					<div class="c-ttl1">
						<h3>Lorem, ipsum.</h3>
					</div>
					<p>Lorem ipsum dolor sit.</p>
				</div>
				<div class="p-form__box1">
					<div class="c-ttl1 is-required">
						<h3>Lorem, ipsum.</h3>
						<a href=""><span>修正</span></a>
					</div>
					<p>T 1234567<br />Lorem ipsum dolor sit amet.<br />Lorem ipsum dolor sit.<br />Lorem, ipsum dolor sit amet
						consectetur.</p>
				</div>
				<div class="p-form__box1">
					<div class="c-ttl1 is-required">
						<h3>Lorem, ipsum.</h3>
						<a href=""><span>修正</span></a>
					</div>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
				</div>
				<div class="p-form__box1 p-form__box1--bg">
					<div class="c-ttl1 is-required">
						<h3>Lorem, ipsum.</h3>
						<a href=""><span>修正</span></a>
					</div>
					<p>01-234-56-78-90-100000</p>
					<div class="c-ttl1">
						<h3>Lorem, ipsum.</h3>
					</div>
					<p>Lorem ipsum dolor sit amet.</p>
					<div class="c-ttl1">
						<h3>Lorem, ipsum.</h3>
					</div>
					<p>Lorem ipsum dolor.</p>
				</div>
				<div class="p-form__box1 p-form__box1--bg">
					<div class="c-ttl1 is-required">
						<h3>Lorem, ipsum.</h3>
						<a href=""><span>修正</span></a>
					</div>
					<p>T 1234567<br />Lorem ipsum dolor sit amet.<br />Lorem ipsum dolor sit.</p>
				</div>
				<div class="p-form__box1">
					<div class="c-ttl1 is-required">
						<h3>Lorem, ipsum.</h3>
						<a href=""><span>修正</span></a>
					</div>
					<p>Lorem ipsum dolor sit.</p>
				</div>
				<div class="p-form__box1">
					<div class="c-ttl1 is-required">
						<h3>Lorem, ipsum.</h3>
						<a href=""><span>修正</span></a>
					</div>
					<p>Lorem ipsum dolor sit.</p>
				</div>
				<div class="p-form__box1">
					<div class="c-ttl1 is-required">
						<h3>Lorem, ipsum.</h3>
						<a href=""><span>修正</span></a>
					</div>
					<p>Lorem ipsum dolor sit.</p>
				</div>
				<div class="c-btn1">
					<input type="submit" value="OK">
				</div>
				<div class="c-btn2">
					<a href=""><span>Cancel</span></a>
				</div>
			</div>
		</form>
	</section>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
