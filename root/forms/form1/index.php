<?php $pageid="form1";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="p-form">
	<p class="pc-only" style="text-align:center; margin: 100px; font-size:3vw">Please change mobile, Thank you !!</p>
	<section class="p-form__1 sp-only">
		<form action method="post">
			<div class="l-container">
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-input1">
					<input type="text" placeholder="Input">
				</div>
				<div class="c-note1">
					<p>Lorem ipsum dolor sit amet.</p>
				</div>
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-input1">
					<input type="text" placeholder="Input">
				</div>
				<div class="c-note1">
					<p>Lorem ipsum dolor sit amet.</p>
				</div>
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-select1">
					<label>
						<select>
							<option value="value1">Value 1</option>
							<option value="value2">Value 2</option>
							<option value="value3">Value 3</option>
							<option value="value4">Value 4</option>
							<option value="value5">Value 5</option>
						</select>
					</label>
				</div>
				<div class="c-box1">
					<div class="c-note1">
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem unde consequuntur nisi.</p>
					</div>
					<div class="c-ttl1">
						<h3>Lorem, ipsum.</h3>
					</div>
					<div class="c-input1">
						<input type="text" placeholder="Input">
					</div>
					<div class="c-ttl1">
						<h3>Lorem, ipsum.</h3>
					</div>
					<div class="c-input1">
						<input type="text" placeholder="Input">
					</div>
					<div class="c-note1">
						<p>Lorem ipsum dolor sit amet.</p>
					</div>
				</div>
				<div class="c-ttl1">
					<h3>Lorem, ipsum.</h3>
				</div>
				<div class="c-input2">
					<input type="tel" placeholder="012">
					<input type="tel" placeholder="345">
					<input type="tel" placeholder="6789">
				</div>
				<div class="c-note1">
					<p>Lorem ipsum dolor sit amet.</p>
				</div>
				<div class="c-btn1">
					<input type="submit" value="OK">
				</div>
			</div>
		</form>
	</section>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
