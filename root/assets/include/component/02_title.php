<?php /*========================================
title
================================================*/ ?>
<div class="c-dev-title1">title</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl1</div>
<div class="l-container">
    <div class="c-ttl1">
        <h3>Lorem, ipsum.</h3>
    </div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl1 is-required</div>
<div class="l-container">
    <div class="c-ttl1 is-required">
        <h3>Lorem, ipsum.</h3>
        <a href=""><span>修正</span></a>
    </div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-ttl1 c-ttl1--red</div>
<div class="l-container">
    <div class="c-ttl1 c-ttl1--red">
        <h3>Lorem, ipsum. <img src="/assets/img/common/icon1.png" alt=""></h3>
    </div>
</div>

