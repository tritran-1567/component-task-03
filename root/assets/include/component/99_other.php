<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-input1</div>
<div class="l-container">
	<div class="c-ttl1">
		<h3>Lorem, ipsum.</h3>
	</div>
	<div class="c-input1">
		<input type="text" placeholder="Input">
	</div>
	<div class="c-note1">
		<p>Lorem ipsum dolor sit amet.</p>
	</div>
	<div class="c-ttl1">
		<h3>Lorem, ipsum.</h3>
	</div>
	<div class="c-input1">
		<input type="text" placeholder="Input">
	</div>
	<div class="c-note1">
		<p>Lorem ipsum dolor sit amet.</p>
	</div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-select1</div>
<div class="c-ttl1">
	<h3>Lorem, ipsum.</h3>
</div>
<div class="c-select1">
	<label>
		<select>
			<option value="value1">Value 1</option>
			<option value="value2">Value 2</option>
			<option value="value3">Value 3</option>
			<option value="value4">Value 4</option>
			<option value="value5">Value 5</option>
		</select>
	</label>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-note1</div>
<div class="c-note1">
	<p>Lorem ipsum dolor sit amet.</p>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-box1</div>
<div class="c-box1">
	<div class="c-note1">
		<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem unde consequuntur nisi.</p>
	</div>
	<div class="c-ttl1">
		<h3>Lorem, ipsum.</h3>
	</div>
	<div class="c-input1">
		<input type="text" placeholder="Input">
	</div>
	<div class="c-note1">
		<p>Lorem ipsum dolor sit amet.</p>
	</div>
	<div class="c-ttl1">
		<h3>Lorem, ipsum.</h3>
	</div>
	<div class="c-input1">
		<input type="text" placeholder="Input">
	</div>
	<div class="c-note1">
		<p>Lorem ipsum dolor sit amet.</p>
	</div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-input2</div>
<div class="c-ttl1">
	<h3>Lorem, ipsum.</h3>
</div>
<div class="c-input2">
	<input type="tel" placeholder="012">
	<input type="tel" placeholder="345">
	<input type="tel" placeholder="6789">
</div>
<div class="c-note1">
	<p>Lorem ipsum dolor sit amet.</p>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-input3</div>
<div class="c-input3">
    <input type="number" placeholder="1234567">
    <div class="c-btn2">
		<a href=""><span>Lorem ipsum</span></a>
	</div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-input4</div>
<div class="c-input4">
	<input type="number" placeholder="01">
	<input type="number" placeholder="234">
	<input type="number" placeholder="56">
	<input type="number" placeholder="78">
	<input type="number" placeholder="90">
	<input type="number" placeholder="100000">
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-input5</div>
<div class="c-input5">
	<input type="number" placeholder="12">
	<input type="number" placeholder="123">
	<input type="number" placeholder="1234">
	<input type="number" placeholder="1234">
	<input type="number" placeholder="1234">
	<input type="number" placeholder="1234">
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-input6</div>
<div class="c-input6">
	<input type="number" placeholder="12">
	<input type="number" placeholder="123">
	<input type="number" placeholder="12">
	<input type="number" placeholder="12">
	<input type="number" placeholder="12">
	<input type="number" placeholder="123456">
</div>
